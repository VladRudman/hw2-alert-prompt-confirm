
"use strick";

/*  1.Які існують типи даних у Javascript?
1)number; 2)string; 3)boolean; 4)null; 5)underfined; 6) object; 7)symbol; 8)bigint

2.У чому різниця між == і ===?
== - не строгое равно, т.е. при сравнении не будет учитываться тип данных, например 1 == "1" (true)
=== - строгое равно, т.е. при сравнении буде учитываться тип данных, например 1 === "1" (false), a 1 === 1 (true)
3.Що таке оператор? 
оператор - это то, что необходимо сделать с операндами.
*/





// Завдання
// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).



let userName = prompt("print your name");
let userAge = prompt("print your age");

if ((userAge === null || userAge === "" || isNaN(userAge)) || ((userName === null || userName === ""))) {
    while ((userAge === null || userAge === "" || isNaN(userAge)) || (userName === null || userName === "")) {
        userAge = prompt("print your age", userAge);
        userName = prompt("print your name", userName);
        continue
    }
    }
    if (userAge < 18) {
        alert("You are not allowed to visit this website");
    }
    else if (userAge >= 18 && userAge <= 22) {
        let question = confirm("Are you sure you want to continue?");
        if (question == true) {
            alert("Welcome, " + userName);
        }
        else (alert("You are not allowed to visit this website"));
    }
    else {
        alert("Welcome, " + userName);
    }









